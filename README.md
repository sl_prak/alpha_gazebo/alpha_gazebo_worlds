# alpha_gazebo_worlds

ROS2 пакет с моделями миров gazebo и `*.launch.py` файлами для их запуска в симуляторе.

## Установка вне контейнера

В директории workspace/src выполните:

```bash
#in your workspace/src folder:
git clone https://gitlab.com/sl_prak/alpha_gazebo_worlds.git
cd ..
rosdep install --from-paths src --ignore-src -r -y
colcon build
```

При использовании [контейнера](https://gitlab.com/sl_prak/alpha_vehicle_simulator) нужно только склонировать репозиторий и собрать проект после сборки docker образа ([подробнее о сборке в контейнере](https://gitlab.com/sl_prak/alpha_vehicle_simulator/-/blob/develop/README.md)).

## Запуск

Запуск пустого мира

```bash
ros2 launch alpha_gazebo_worlds run_empty_world.launch.py
```

Пример запуска пользовательского мира

```bash
ros2 launch alpha_gazebo_worlds alpha_gazebo_worlds.launch.py world_file_name:='car.world'
```

Если вы хотите запустить свой мир, переместите его в директорию `<path_to_alpha_gazebo_worlds>/worlds` и укажите его при по примеру выше.
