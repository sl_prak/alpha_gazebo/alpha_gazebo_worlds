
import os

from ament_index_python.packages import get_package_share_directory, get_package_prefix
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration


def generate_launch_description():
    pkg_path = get_package_prefix("alpha_gazebo_worlds").replace("/install/", "/src/")

    world_file_path = [pkg_path, "/worlds/", LaunchConfiguration('world_file_name')]

    share_pkg_path = get_package_share_directory("alpha_gazebo_worlds")

    run_world_launch_description = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(os.path.join(share_pkg_path,
                                                   "launch",
                                                   "run_empty_world.launch.py")))

    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "world_file_name", 
                default_value="car.world",    
                description="World name in alpha_gazebo_worlds/worlds."),            
            DeclareLaunchArgument("world", default_value=world_file_path),
            DeclareLaunchArgument("verbose", default_value="false"),
            run_world_launch_description
        ]
    )
