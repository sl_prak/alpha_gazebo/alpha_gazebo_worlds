
import os

from ament_index_python.packages import get_package_share_directory, get_package_prefix
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource


def generate_launch_description():

    gazebo_ros_pkg_path = get_package_share_directory("gazebo_ros")

    gazebo_launch_description = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(os.path.join(gazebo_ros_pkg_path,
                                                   "launch",
                                                   "gazebo.launch.py")))

    return LaunchDescription(
        [
            DeclareLaunchArgument("world", default_value="worlds/empty.world"),
            DeclareLaunchArgument("verbose", default_value="true"),
            gazebo_launch_description
        ]
    )
